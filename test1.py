from psychopy import visual, core #import some libraries from PsychoPy

#create a window
mywin = visual.Window([1024, 768],monitor='testMonitor', units='deg')

#create some stimuli
grating = visual.GratingStim(win=mywin, sf=4)

trialClock = core.Clock()
t = 0
while t < 10:    # quits after 20 secs
	t = trialClock.getTime()

	grating.setPhase(1*t)  # drift at 1Hz
	grating.draw()  #redraw it

	mywin.flip()          #update the screen

#cleanup
#mywin.close()
core.quit()




# from psychopy import visual, core, event

# #create a window to draw in
# myWin = visual.Window((600,600), allowGUI=False)

# #INITIALISE SOME STIMULI
# grating1 = visual.GratingStim(myWin, mask="gauss",
# 							  color=[1.0, 1.0, 1.0],
# 							  opacity=1.0,
# 							  size=(1.0, 1.0),
# 							  sf=(4,0), ori=45)

# grating2 = visual.GratingStim(myWin, mask="gauss",
# 							  color=[1.0, 1.0, 1.0],
# 							  opacity=0.5,
# 							  size=(1.0, 1.0),
# 							  sf=(4,0), ori=135)

