import pygame
# import pygame.locals
from pygame.locals import *
import time

from OpenGL.GL import *
from OpenGL.GL import shaders

def vertex_shader():
	shader = """
attribute vec2 a_pos;
void main() {
	gl_Position = vec4(a_pos, 0, 1);
}
	"""
	return shaders.compileShader(shader, GL_VERTEX_SHADER)

def fragment_shader():
	shader = """
uniform vec2 u_resolution;
uniform float u_time;

void main() {
	vec2 st = gl_FragCoord.xy/u_resolution.xy;
	st.x *= u_resolution.x/u_resolution.y;

	st.x = st.x + u_time/6.0;///1.0 - floor(u_time/1.0)

	float clr = st.x/0.1 - floor(st.x/0.1);
	clr = floor(clr+0.5);
	gl_FragColor = vec4(clr,clr,clr,1.0);
}
	"""
	return shaders.compileShader(shader, GL_FRAGMENT_SHADER)

	


def main():
	pygame.init()
	display = (1280,1024)
	pygame.display.set_mode(display, DOUBLEBUF|OPENGL)

	vert = vertex_shader()
	frag = fragment_shader()
	shade = shaders.compileProgram(vert, frag)
	glUseProgram(shade)

	a_pos = glGetAttribLocation(vert, 'a_pos');
	glBindBuffer(ARRAY_BUFFER, glCreateBuffer());
	glBufferData(ARRAY_BUFFER, np.array([-1, -1, 1, -1, -1, 1, -1, 1, 1, -1, 1, 1], dtype=np.float32), STATIC_DRAW);
	glEnableVertexAttribArray(a_pos);
	glVertexAttribPointer(a_pos, 2, FLOAT, False, 0, 0);

	uniform_locs = [
		glGetUniformLocation(shade, 'u_resolution'),
		glGetUniformLocation(shade, 'u_time'),
	]

	glUniform2f(uniform_locs[0], 1280, 1024)
	start = time.time()
	while True:
		for event in pygame.event.get():
			if event.type == pygame.QUIT:
				pygame.quit()
				quit()


		glUniform1f(uniform_locs[1], time.time() - start)

		# glEnableClientState(GL_VERTEX_ARRAY);
		# glEnableClientState(GL_COLOR_ARRAY);
		# glVertexPointer(3, GL_FLOAT, 24, self.vbo )
		# glColorPointer(3, GL_FLOAT, 24, self.vbo+12 )
		glUseProgram(shade)
		glDrawArrays(GL_TRIANGLES, 0, 6)

		pygame.display.flip()
		pygame.time.wait(10)

	glDisableClientState(vert);
	glDisableClientState(frag);

main()