import pygame
from pygame.locals import *
 
from OpenGL.GL import *
from OpenGL.GLUT import *
 
if __name__ == '__main__':
    glutInit(sys.argv)
    width, height = 640, 480
    pygame.init()
    pygame.display.set_mode((width, height), OPENGL | DOUBLEBUF)
    print glGetString(GL_VERSION)